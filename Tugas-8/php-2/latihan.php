<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function PHP</title>
</head>
<body>

<?php
// contoh operator assignment
$panjang = 10; // contoh assignment
$lebar = 8; // contoh assignment
$luas = $panjang * $lebar ; // contoh assignment juga
echo "$panjang * $lebar = $luas <br>";
// tanda '=' mewakili proses assignment,
// ada beberapa tanda lagi yang dapat
// melakukan assignment,
// assignment penjumlahan diwakili tanda '+=' contohnya
$tambahdua=2;
$tambahdua += 2;
echo "$tambahdua = $tambahdua <br>";
// hasilnya 2
// hal ini sama dengan pernyatan :
// $tambahdua = $tambahdua + 2;
// assignment pengurangan diwakili tanda '-=' contohnya
$clip = 22;
$clip -= 2; // sama dengan $clip = $clip - 2;
echo "$clip = $clip <br>";
?>

<br>
<?php
$angka = 10;
echo $angka == 10; // true atau 1
echo "<br>";
var_dump($angka == 9); // bool(false) atau 0
echo "<br>";
var_dump($angka == 10);// bool(false) atau 0
?>

<br>
<?php
$sifat= "rajin";
var_dump($sifat != "malas"); // bool(true)
echo "<br>";
echo $sifat <> "bandel"; // 1
?>

<br>
<?php
$angka = 10;
var_dump($angka === "10");// bool(false)
echo "<br>";
var_dump($angka === 10);// bool(true)
?>

<br>
<?php
$angka = 17;
echo "<br>angka $angka";
echo "<br>angka <20 "; var_dump($angka < 20); // bool(true)
echo "<br>angka <17 "; var_dump($angka <= 17); // bool(true)
echo "<br>angka <15 "; var_dump($angka >= 15); // bool(true)
echo "<br>angka <12 "; var_dump($angka > 12); // bool(true)
?>

<br>
<?php
echo "<br>true && true "; var_dump(true && true); // bool(true)
echo "<br>true && false "; var_dump(true && false);// bool(false)
echo "<br>false && true "; var_dump(false && true);// bool(false)
echo "<br>false && false "; var_dump(false && false);// bool(false)

echo "<br>true || true "; var_dump(true || true); // bool(true)
echo "<br>true || false "; var_dump(true || false); // bool(true)
echo "<br>false || true "; var_dump(false || true); // bool(true)
echo "<br>false || false "; var_dump(false || false); // bool(false)
?>

<br>
<!-- if (<kondisi benar>) {
   // blok kode ini akan dieksekusi jika kondisi benar
} else {
   // blok kode ini akan dieksekusi jika kondisi salah
}

Contoh nya seperti ini: -->
<?php
echo "<br>";
$hari_ini = "senin";
echo "jika variabel hari ini = $hari_ini maka I Love Monday<br>";
echo "variabel hari ini = $hari_ini <br>";
if($hari_ini == "senin") {
  echo "I Love Monday<br>";
} else {
    echo "Ini bukan hari senin<br>";
}
?>

<br>
<?php
function printBr()
{
    echo "<br>";
}

$materi= "PHP";
echo ($materi == "PHP")?"<br>Hari ini belajar PHP<br>": "<br>Hari ini bukan materi PHP<br>";
?>

<!-- Function -->
<?php

$nama='Gilang';
function kenalkan($nama) {
  echo "kenalkan nama saya $nama";
}

// panggil function kenalkan
kenalkan($nama);


function buat_nama_kapital($nama) {
    return ucwords($nama);
  }

printBr();
  $nama_lengkap = buat_nama_kapital("gilang pangestu");
  printBr();
  echo $nama_lengkap; // "gilang pangestu"
  
  printBr();
  // memanggil function di dalam function lain
  function perkenalan($nama, $asal) {
    echo "perkenalkan nama saya ". buat_nama_kapital($nama). " asal saya dari $asal";
  }
    perkenalan("Sirtyo", "Bandung");
?>


<!-- for(nilai awal; batas nilai; operator increment/decrement)
{
pernyataan yang akan di proses
}
contohnya : -->
<?php
printBr();
printBr();
for($i = 1; $i < 5 ; $i++ ){
  echo "ini adalah looping ke $i <br>";
}
?>


<!-- while (Kondisi) {
  pertanyaan yang akan di proses;
}
 contohnya : -->
<?php
printBr();
$x = 1;
while($x <= 5) {
  echo "$x - satu Sampai lima<br>";
  $x++;
} 
?>


<!-- do {
  pertanyaan yang akan diproses;
} while (kondisi);
contohnya : -->
<?php
printBr();
$x = 1;
do {
  echo "$x - satu Sampai lima<br>";;
  $x++;
} while ($x <= 5);
?>

<!-- Foreach Loop
sintaks foreach do seperti berikut:
foreach ($array as $value) {
  pertanyaan yang akan diproses;
}
 contohnya : -->
<?php
printBr();
$animals = array("cat", "dog", "snake", "ant", "lion"); 
foreach ($animals as $animal) {
  echo "$animal <br>";
}
?>  

<!-- contoh lainnya: -->

<?php
printBr();
$age = array("Rezky"=>"25", "Abduh"=>"29", "Iqbal"=>"33");
foreach($age as $x => $val) {
  echo "$x = $val<br>";
}
?>





</body>
</html>