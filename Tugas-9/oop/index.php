<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<?php
require_once "animal.php";
require_once "ape.php";
require_once "frog.php";

$shaun = new animal("Shaun");
echo "Nama : " . $shaun->name . "<br>";
echo "Legs : " . $shaun->legs . "<br>";
echo "Cold Blood : " . $shaun->cold_blood . "<br>";

echo "<br>";

$sungokong = new ape("Sungokong");
echo "Nama : " . $sungokong->name . "<br>";
echo "Legs : " . $sungokong->legs . "<br>";
echo "Cold Blood : " . $sungokong->cold_blood . "<br>";
echo "Yell : ";
$sungokong->yell() . "<br>";

echo "<br>";
echo "<br>";

$kodok = new frog("Buduk");
echo "Nama : " . $kodok->name . "<br>";
echo "Legs : " . $kodok->legs . "<br>";
echo "Cold Blood : " . $kodok->cold_blood . "<br>";
echo "Jump : ";
$kodok->yell() . "<br>";
?>

</body>
</html>
