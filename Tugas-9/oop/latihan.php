<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<!-- belajar class -->
<?php
// class Mobil 
// {
//   public $roda = 4;
//   public function jalan()
//   {
//     echo "Mobil berjalan<br>";
//   }
// }
?>

<!-- membuat object  -->
<?php
// $mini = new Mobil();
// $mini->jalan(); // menampilkan echo 'Mobil berjalan'
// echo $mini->roda . "<br>"; // 4
// class Motor {
//     protected $roda = 2;
//     public function jumlah_roda() {
//       echo $this->roda . " <br>";
//     }
//   }
//   $mini = new Motor;
//   $mini->jumlah_roda(); // 4 
?>


<!-- Private Method -->
<!-- <?php
// class Pesawat
// {
//   Private $roda = 3;
//   Private function jalan()
//   {
//     echo 'Pesawat berjalan';
//   }
// } 
// $avanza = new Pesawat();
// echo $avanza->jalan();
// echo PHP_EOL;
// echo $avanza->roda;
// echo PHP_EOL;
?> -->

<!-- Protected Method -->
<?php
// class Sepeda 
// {
//   protected $roda = 4;
// }
// class SepedaSport extends Sepeda
// {
//   protected $maxSpeed;
// }
// $Polygon = new SepedaSport;
// echo $Polygon->roda ; // 4 
?>

<!-- Public Method -->
<?php
// class Mobil
// {
//   private $roda = 4;
//   public function jumlahRoda()
//   {
//     echo $this->roda;
//   }
// }
// $kijang = new Mobil();
// $kijang->jumlahRoda(); // menampilkan 4
?>

<!-- Constructor -->
<?php

class Mobil {
  protected $roda= 4;
  public $merk;
  public function __construct($merk) 
  {
    $this->merk= $merk;
  }
}
$xeniya = new Mobil("Xeniya");

echo $xeniya->merk; // Xeniya
?>



</body>
</html>