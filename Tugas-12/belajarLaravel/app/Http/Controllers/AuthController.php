<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register (){
        return view ('register');
    }
    
    public function welcome (){
        return view ('welcome');
    }
    
    public function biodata (){
        return view ('halaman.biodata');
    }

    public function kirim (Request $request){ 
        $name = $request['name'];
        $pass = $request['pass'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $language = $request['language'];
        $message = $request['message'];

        return view ('welcome',
        ['name'=>$name,
        'pass'=>$pass,
        'gender'=>$gender,
        'nationality'=>$nationality,
        'language'=>$language,
        'message'=>$message]);
      }
}
