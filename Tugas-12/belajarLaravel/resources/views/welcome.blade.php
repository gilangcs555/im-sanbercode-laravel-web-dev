<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> [endif]-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Welcome</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="">
    </head>
    <body>
    
    <h1>SELAMAT DATANG!</h1>
{{$name}} dari
@if ($nationality === "1") Indonesia
@elseif ($nationality === "2") English
@elseif ($nationality === "3") Other
@elseif ($nationality === "1" and $nationality === "2") Indonesia, English
@elseif ($nationality === "1" and $nationality === "2" and $nationality === "3") Indonesia, English, Other
@elseif ($nationality === "1" and $nationality === "3") Indonesia, Other
@elseif ($nationality === "2" and $nationality === "3") English, Other
@endif

dengan jenis kelamin 
@if ($gender === "1") Laki - laki
@elseif ($gender === "2") Perempuan
@else Other
@endif

dan bahasa 
Bahasa :
@if ($language === "1") Indonesia
@elseif ($language === "2") English
@elseif ($language === "3") Other
@endif

memiliki pesan {{$message}}

<h3>Terima kasih telah bergabung di Sanberbook.
    Social Media kita bersama!</h3>
    <li>Home <a href="/">Disini</a></li>
        <script src="" async defer></script>
    </body>
</html>