<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <![endif]-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Biodata</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="">
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <h1>Halaman Biodata</h1>
        Nama : {{$name}} <br>

        Jenis Kelamin :
        @if ($gender === "1") Laki - laki
        @elseif ($gender === "2") Perempuan
        @else Other
        @endif
            
        Kebangsaan :
        @if ($nationality === "1") Indonesia
        @elseif ($nationality === "2") English
        @elseif ($nationality === "3") Other
        @elseif ($nationality === "1" and $nationality === "2") Indonesia, English
        @elseif ($nationality === "1" and $nationality === "2" and $nationality === "3") Indonesia, English, Other
        @elseif ($nationality === "1" and $nationality === "3") Indonesia, Other
        @elseif ($nationality === "2" and $nationality === "3") English, Other
        @endif

        Bahasa :
        @if ($language === "1") Indonesia
        @elseif ($language === "2") English
        @elseif ($language === "3") Other
        @elseif ($language === "1" and $language === "2") Indonesia, English
        @elseif ($language === "1" and $language === "2" and $language === "3") Indonesia, English, Other
        @elseif ($language === "1" and $language === "3") Indonesia, Other
        @elseif ($language === "2" and $language === "3") English, Other
        @endif

        Pesan : {{$message}}

        <a href="/"> Sanbercode</a>
        <script src="" async defer></script>
    </body>
</html>