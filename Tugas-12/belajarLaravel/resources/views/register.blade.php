<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <![endif]-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Register</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="">
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <h1>Buat Account Baru</h1>

<h3>Sign Up Form</h3>

<form action="/kirim" method="post">
    @csrf
    <label>First Name:</label><br>
    <input type="text" name="name"><br><br>
    <label>Last Name:</label><br>
    <input type="text" name="pass"><br><br>
    <label>Gender:</label><br>
    <input type="radio" name="gender" value="1">Male<br>
    <input type="radio" name="gender" value="2">Female<br>
    <input type="radio" name="gender" value="3">Other<br><br>
    <label>Nationality:</label><br>
    <select name="nationality">
        <option value="1">Indonesia</option>
        <option value="2">English</option>
        <option value="3">Other</option>
    </select><br><br>
    <label>Language Spoken</label><br>
    <input type="checkbox" name= "language" value="1">Bahasa Indonesia<br>
    <input type="checkbox" name= "language" value="2">English<br>
    <input type="checkbox" name= "language" value="3">Other<br>
    <br><br>
    <label>Bio:</label><br>
    <textarea name="message" rows="10" cols="30"></textarea><br><br>
    <input type="submit" value=kirim>

</form>

        <script src="" async defer></script>
    </body>
</html>